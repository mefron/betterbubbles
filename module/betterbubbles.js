import { BETTERBUBBLES } from "../config.js";
CONFIG.betterbubbles = {};

/*
 *****************************************************
 */
Handlebars.registerHelper("truncate", function (inputstring, start, end) {
  var truncated = inputstring.substring(start, end);
  return new Handlebars.SafeString(truncated);
});

/*
 *****************************************************
 */
function _objectInspector(object, result) {
	if (typeof object != "object") {
		return "Invalid object";
	}

	if (typeof result == "undefined") {
		result = '';
	}

	if (result.length > 50) {
		return "[RECURSION TOO DEEP. ABORTING.]";
	}

	var rows = [];
	for (var property in object) {
		var datatype = typeof object[property];

		var tempDescription = result + '"' + property + '"';
		tempDescription += ' (' + datatype + ') => ';
		if (datatype == "object") {
			tempDescription += 'object: ' + _objectInspector(object[property], result + '\t') + '// "' + property + '"\n';
		} else if (datatype == "function") {
			tempDescription += 'function';
			tempDescription = '';
		} else {
			tempDescription += object[property];
		}

		if (tempDescription > '') {
			rows.push(tempDescription);
		}
	}

	return '{\n' + rows.join("\n") + '\n' + result + '}';
}

/*
 *****************************************************
 */
function _displayPool(part, keep) {
	let rollString = '';
	let color = '#FFFFFF'
	if (!keep) {
		color = "#AAAAAA";
	}
	part.results.forEach((roll, index) => {
		if (roll.result == part.faces && keep) {
			color = '#00A676';
		} else if (roll.result == 1 && index == 0) {
			color = '#C84630';
		}
		rollString += `<span class="roll" style="background-image: url('/modules/betterbubbles/assets/d${part.faces}.png');"><span style="color:${color};">${roll.result}</span></span>`; 
	});

	return rollString;
}

/*
 *****************************************************
 */
function _displayRoll(part) {
	let rollString = '';
	part.results.forEach((roll) => {
		rollString += `<span class="roll" style="background-image: url('/modules/betterbubbles/assets/d${part.faces}.png');"><span>${roll.result}</span></span>`; 
	});

	return rollString;
}

/*
 *****************************************************
 		{{#if data.flags.betterrolls5e}}
			{{#each data.flags.betterrolls5e.entries}}
				{{#if this.title}}
					{{this.title}}
				{{/if}}

				{{#each this.entries}}
					{{#if this.roll.total}}
						ROLL: {{this.roll.total}}
					{{/if}}
					{{#if this.total}}
						TOTAL: {{this.total}}
					{{/if}}
					{{#if this.baseRoll.total}}
						B-ROLL: {{this.baseRoll.total}}
					{{/if}}
					{{#if this.damageType}}
						DamageType: {{this.damageType}}
					{{/if}}
				{{/each}}
			{{/each}}
			
			
				<div class="flavor">
		<i class="icon fa fa-dice-d20"></i>
		{{{betterPart this}}}
	</div>

 */
Handlebars.registerHelper("betterPart", function (part) {
	let flavor = '';
	let roll = '';
	let damage = [];
	let crit = '';

	if (part.data.flags.betterrolls5e) {
		let br = part.data.flags.betterrolls5e;

//		let r = _objectInspector(br, '');
//		console.log('part: ' + r);

		for (var ent in br.entries) {
			let obj = br.entries[ent];
			var datatype = typeof obj;

			if (datatype == "object") {
				let r = _objectInspector(obj, '');
				console.log('ENT[' + ent + ']: ' + r);

				if (obj.title) {
					flavor += '&nbsp;' + obj.title;
				}
			}

			for (var key1 in obj.entries) {
				let obj1 = obj.entries[key1];
				if (obj1.total && !obj1.ignored) {
//					let r = _objectInspector(obj1, '');
//					console.log('obj1: ' + r);

					if (typeof obj1.critType == 'string') {
						crit = obj1.critType;
					}

					roll = '[' + obj1.total + ']';
				}

				if (obj1.baseRoll && obj1.baseRoll.total) {
					let dmgTotal = obj1.baseRoll.total;

					if (obj1.critRoll && obj1.critRoll.total) {
						dmgTotal += obj1.critRoll.total;
					}

					let dmgType = obj1.damageType ? obj1.damageType : 'damage';

					damage.push(dmgTotal + '&nbsp;' + dmgType);
				}
			}
		}
	} else {
		let r = _objectInspector(part, '');
		console.log('Unknown part: ' + r);
	}

	let flavorhtml = '<span class="flavor">' + flavor + '</span>';
	let rollhtml = '<span class="roll ' + crit + '">' + roll + '</span>';
	let damagehtml = '<div class="damage">' + damage.join('\n') + '</div>';

//	console.log('Display: ' + CONFIG.betterbubbles.displayOption);

//	console.log('Flavor: ' + flavorhtml);
//	console.log('Roll: ' + rollhtml);
//	console.log('Damage: ' + damagehtml);

	switch (CONFIG.betterbubbles.displayOption) {
		case "flavor":
			return '<div><i class="icon fa fa-dice-d20"></i>' + flavorhtml + '</div>';

		case "results":
			return '<div><i class="icon fa fa-dice-d20"></i>&nbsp;' + rollhtml + '</div><div>' + damagehtml + '</div>';

		case "details":
			return '<div><i class="icon fa fa-dice-d20"></i>' + flavorhtml + '&nbsp;' + rollhtml + '</div><div>' + damagehtml + '</div>';

		default:
			return '<div><i class="icon fa fa-dice-d20"></i>' + flavorhtml + '&nbsp;' + rollhtml + '</div><div>' + damagehtml + '</div>';
	}

	return '<div><i class="icon fa fa-dice-d20"></i>' + flavorhtml + '&nbsp;' + rollhtml + '</div><div>' + damagehtml + '</div>';
});

/*
 *****************************************************
 */
Handlebars.registerHelper("rollPart", function (part) {
	console.log('rollPart Helper called');

	if (part instanceof Die) {
		return _displayRoll(part);
	} else if (part instanceof DicePool) {
		let poolString = '';
		part.dice.forEach(die => {
			if (part.modifiers == 'kh' && die.total == part.total) {
				poolString += _displayPool(die, true);
			} else {
				poolString += _displayPool(die, false);
			}
		});

		return poolString;
	} else {
		return part;
	}
});

/*
 *****************************************************
 */
Hooks.once("init", async function () {
	// Localize labels
	BETTERBUBBLES.displayOptions = Object.entries(BETTERBUBBLES.displayOptions).reduce(
		(obj, e) => {
			obj[e[0]] = game.i18n.localize(e[1]);
			return obj;
		},
		{}
	);

	game.settings.register("betterbubbles", "BetterBubblesDisplay", {
		name: game.i18n.localize("BETTERBUBBLES.SettingsName"),
		hint: game.i18n.localize("BETTERBUBBLES.SettingsHint"),
		scope: "client",
		config: true,
		choices: BETTERBUBBLES.displayOptions,
		default: "details",
		type: String,
		onChange: (value) => {
			CONFIG.betterbubbles.displayOption = value;
		},
	});

	CONFIG.betterbubbles.template = '/modules/betterbubbles/templates/bubble-template.html';

	CONFIG.betterbubbles.displayOption = game.settings.get(
		"betterbubbles",
		"BetterBubblesDisplay"
	);
});

/*
 *****************************************************
 */
Hooks.on("renderChatMessage", async function (msg, html) {
	// Don't display old chat messages at refresh
	if (Date.now() - msg.data.timestamp > 500) {
		return;
	}

	if (msg.data.speaker.token && msg.isRoll && msg.isContentVisible) {
		const tok = canvas.tokens.placeables.find(
			(t) => (t.id == msg.data.speaker.token)
		);
		if (tok) {
			if (msg.data.flags.betterrolls5e) {
//				console.log('betterrolls found');

//				let r = _objectInspector(msg.data.flags.betterrolls5e, '');
//				console.log('msg.data.flags.betterrolls5e: ' + r);

				renderTemplate(CONFIG.betterbubbles.template, msg).then((html) => {
					canvas.hud.bubbles.say(tok, html, false);
				});
			} else {
				console.log('No betterrolls');

				let doc = new DOMParser().parseFromString(msg.data.flavor, 'text/html');

				let r = _objectInspector(msg.data, '');
				console.log('msg.data:' + r);

				msg.data.flavor = doc.body.textContent;
				renderTemplate(CONFIG.betterbubbles.template, msg).then((html) => {
					canvas.hud.bubbles.say(tok, html, false);
				});
			}
		}
	}
});
