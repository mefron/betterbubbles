export const BETTERBUBBLES = {};

BETTERBUBBLES.templates = {
	"/modules/betterbubbles/templates/bubble-details.html": "BETTERBUBBLES.RollDetailsTemplate",
	"/modules/betterbubbles/templates/bubble-flavor.html": "BETTERBUBBLES.RollFlavorTemplate",
	"/modules/betterbubbles/templates/bubble-result.html": "BETTERBUBBLES.RollResultTemplate"
};

BETTERBUBBLES.displayOptions = {
	"details": "BETTERBUBBLES.Details",
	"flavor": "BETTERBUBBLES.FlavorOnly",
	"results": "BETTERBUBBLES.ResultsOnly"
};
